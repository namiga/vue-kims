import sys0000 from './sys/sys0000.vue'
import man0010 from './man/man0010.vue'
import man0020 from './man/man0020.vue'
import man0030 from './man/man0030.vue'
import acc0010 from './acc/acc0010.vue'
import acc0020 from './acc/acc0020.vue'
import acc0030 from './acc/acc0030.vue'
import acc0040 from './acc/acc0040.vue'
import acc0050 from './acc/acc0050.vue'

const plugin = {
  install(Vue) {
    Vue.component(sys0000.name, sys0000)
    Vue.component(man0010.name, man0010)
    Vue.component(man0020.name, man0020)
    Vue.component(man0030.name, man0030)
    Vue.component(acc0010.name, acc0010)
    Vue.component(acc0020.name, acc0020)
    Vue.component(acc0030.name, acc0030)
    Vue.component(acc0040.name, acc0040)
    Vue.component(acc0050.name, acc0050)
  }
}

export default plugin
