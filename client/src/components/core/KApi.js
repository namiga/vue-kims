import axios from 'axios'

const baseURI = 'http://localhost:8080/api'
let api = {}
// eslint-disable-next-line
let token = ''

// 토큰설정
api.setToken = (newToken) => {
  token = newToken
}

// 인증
api.login = (email, password) => {
  return new Promise((resolve, reject) => {
    axios.request({
      method: 'get',
      url: `${baseURI}/auth/login.json`,
      data: { 'username':email, 'password':password }
    }).then((res) => {
      if (res.data.success) {
        token = res.token
        resolve(res.data)
      }
      reject(res.data.message)
    }).catch((err => {
      reject(err.message)
    }))
  })
}

// 로그아웃
api.logout = (email) => {
  return new Promise((resolve, reject) => {
    axios.request({ method: 'get', url: `${baseURI}/auth/login.json`, data: { 'username': email } })
      .then((res) => {
        if (res.data.success) {
        token = res.token
        resolve(res.data)
      }
      reject(res.data.message)
    }).catch((err => {
      reject(err.message)
    }))
  })
}

// 사이드바 메뉴 구하기
api.getMenu = () => {
  return new Promise((resolve, reject) => {
    axios.request({ method: 'get', url: `${baseURI}/system/menu.json` })
      .then((res) => { resolve(res.data) })
      .catch((err) => { console.error(err) })
  })
}

export default api
