import '@fortawesome/fontawesome-free/css/all.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueMoment from 'vue-moment'
import moment from 'moment'
import SnacbarStackPlugin from 'snackbarstack'
import App from './App.vue'
import KApi from './components/core/KApi'
import plugin from './components'

// moment 사용
require('moment/locale/ko')
Vue.use(VueMoment, { moment })
// stacked snackbar 사용
Vue.use(SnacbarStackPlugin)
// api 호출용
Vue.prototype.$api = KApi

Vue.use(Vuetify, {
 iconfont: 'fa',
 icons: {
   'login': 'fas fa-sign-in-alt',
   'logout': 'fas fa-sign-out-alt'
 }
})

Vue.use(plugin)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
